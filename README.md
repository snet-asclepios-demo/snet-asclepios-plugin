# snet-asclepios-plugin

XNAT plugin providing SleepResearchSession and encPsgScanData schemas and views.


## Prerequisites

* A running deployment of the ASCLEPIOS stack (see [snet-aclepios-docker-compose](https://gitlab.com/indie-sleep-demo/snet-asclepios-docker-compose))

* snet browser clients installed on tomcat (see [the snet-xnat-asclepios Dockerfile](https://gitlab.com/indie-sleep-demo/dockerfiles/-/tree/master/snet-xnat-asclepios))
    * XNAT must be available at `/xnat` rather than `/` on tomcat
    * [snet-asclepios-search](https://gitlab.com/indie-sleep-demo/snet-asclepios-search) must be availe at `/asclepios-search`
    * [copla-editor](https://gitlab.com/indie-sleep-demo/copla-editor) must be availe at `/sn-editor`

## build

* building has been tested with
    * gradle 6.0.1
    * openjdk 11

```
./gradlew jar 
```

## install

```
cp build/libs/snet02-asclepios-plugin-x.x.x.jar /path/to/xnat/plugins
```

## configure

* login to xnat
* go to `Administer -> Data Types -> Set Up Additional Data types` and add all the `snet02:*` data-types.

## Usage

* once installed, two new datatype will be available in XNAT: `SleepResearchSession` and `encPsgScanData` and `encPsgAnnotationData`
* use snet-editor to upload encrypted bio-signals to ASCLEPIOS storage and index them in XNAT.
* The bio-signals and their metadata should now be available to download and decrypt in the XNAT admin interface.
